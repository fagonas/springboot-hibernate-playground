package hello.model.one_one;

import hello.model.PersonBase;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class Author extends PersonBase implements Serializable {

    private static final long serialVersionUID = 1L;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn()
    private Phone phone;

    public Phone getPhone() {
        return phone;
    }

    public void setPhone(Phone phone) {
        this.phone = phone;
    }

    //    @Override
//    public String toString() {
//        return "author: " + this.getId() + " " + this.getName() + " " + this.getDob();
//    }
}
