package hello;

import hello.model.one_one.Author;
import hello.model.one_one.Phone;
import hello.repositories.AuthorRepository;
import hello.repositories.CustomerRepository;
import hello.repositories.PhoneRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.Date;

@SpringBootApplication
public class Application {

	private static final Logger log = LoggerFactory.getLogger(Application.class);

	public static void main(String[] args) {
		SpringApplication.run(Application.class);
	}

	@Bean
	public CommandLineRunner demo(CustomerRepository repository, AuthorRepository authorRepository, PhoneRepository phoneRepository) {
		return (args) -> {
			// save a couple of customers
//			repository.save(new Customer("Jack", "Bauer"));
//			repository.save(new Customer("Chloe", "O'Brian"));
//			repository.save(new Customer("Kim", "Bauer"));
//			repository.save(new Customer("David", "Palmer"));
//			repository.save(new Customer("Michelle", "Dessler"));
//
//			// fetch all customers
//			log.info("Customers found with findAll():");
//			log.info("-------------------------------");
//			for (Customer customer : repository.findAll()) {
//				log.info(customer.toString());
//			}
//			log.info("");
//
//			// fetch an individual customer by ID
//			Customer customer = repository.findOne(1L);
//			log.info("Customer found with findOne(1L):");
//			log.info("--------------------------------");
//			log.info(customer.toString());
//			log.info("");
//
//			// fetch customers by last name
//			log.info("Customer found with findByLastName('Bauer'):");
//			log.info("--------------------------------------------");
//			for (Customer bauer : repository.findByLastName("Bauer")) {
//				log.info(bauer.toString());
//			}
//			log.info("");

            Author author = new Author();
            author.setName("james bonn");
            author.setDob(new Date());
//            authorRepository.save(author);
//            log.info("finish saving author");

//            Author author1 = authorRepository.findOne(1l);
//            log.info("find author: " + author1.getId() + " " + author1.getName() +  " " + author1.getDob());

            Phone phone = new Phone();
//            phone.setAuthor(author);
            phone.setNumber("12345678");
//            phoneRepository.save(phone);
            author.setPhone(phone);
            authorRepository.save(author);
//            Phone phone1 = phoneRepository.findOne(1l);
//            log.info(phone1.toString());

		};
	}

}
